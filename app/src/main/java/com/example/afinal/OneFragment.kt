package com.example.afinal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.afinal.databinding.FragmentHomeBinding
import com.example.afinal.databinding.FragmentOneBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private var _binding: FragmentOneBinding? = null
private val binding get() = _binding

/**
 * A simple [Fragment] subclass.
 * Use the [OneFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class OneFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var _binding: FragmentOneBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOneBinding.inflate(inflater,container,false)
        // Inflate the layout for this fragment
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnToHome?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat1?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat1Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat2?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat2Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat3?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat3Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat4?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat4Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat5?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat5Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat6?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat6Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat7?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat7Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat8?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat8Fragment()
            view.findNavController().navigate(action)
        }
        binding?.btnToWat9?.setOnClickListener{
            val action = OneFragmentDirections.actionOneFragmentToWat9Fragment()
            view.findNavController().navigate(action)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment One_Fragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            OneFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}